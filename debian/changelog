libiptables-chainmgr-perl (1.6-2) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-
    Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * Add patch to update path to `iptables'. (LP: #1898970)
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Thu, 08 Oct 2020 18:35:50 +0200

libiptables-chainmgr-perl (1.6-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Martín Ferrari ]
  * Remove myself from Uploaders.

  [ Salvatore Bonaccorso ]
  * debian/control: Remove Franck Joncourt from Uploaders.
    Thanks to Tobias Frost (Closes: #831304)

  [ gregor herrmann ]
  * Remove Fabrizio Regalli from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Import upstream version 1.6
  * Add myself to Uploaders since there were none available remaining
  * Make (Build-)Depends(-Indep) on libiptables-parse-perl unversioned
  * debian/control: Wrap and sort fields
  * Declare compliance with Debian policy 3.9.8
  * Add myself to copyright stanza for debian/* packaging files

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 26 Dec 2016 20:07:08 +0100

libiptables-chainmgr-perl (1.5-1) unstable; urgency=medium

  * Team upload.
  * Update debian/upstream/metadata (typo in Git URL).
  * Import upstream version 1.5.

 -- gregor herrmann <gregoa@debian.org>  Mon, 21 Dec 2015 20:46:51 +0100

libiptables-chainmgr-perl (1.4-1) unstable; urgency=low

  * Team upload

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Florian Schlichting ]
  * Add debian/upstream/metadata
  * Import upstream version 1.4
  * Bump years of upstream copyright
  * Bump dh compat to level 9
  * Declare compliance with Debian Policy 3.9.6
  * Mark package autopkgtest-able

 -- Florian Schlichting <fsfs@debian.org>  Fri, 09 Oct 2015 23:17:39 +0200

libiptables-chainmgr-perl (1.2-1) unstable; urgency=low

  * Imported Upstream version 1.2
  * Bump libiptables-parse-perl to (>= 0.9)

 -- Fabrizio Regalli <fabreg@fabreg.it>  Sun, 04 Mar 2012 22:28:17 +0100

libiptables-chainmgr-perl (0.9.9-1) unstable; urgency=low

  [ Franck Joncourt ]
  * Swith to dh 7
    Updated d.{control,compat,rules}
  * Bumped up Standards-Version to 3.8.3:
    + Removed old versionned perl BD.
  * Refreshed both d.control and d.copyright with my new email address.
  * Switch to dpkg-source 3.0 (quilt) format.

  [ gregor herrmann ]
  * Set Standards-Version to 3.9.1; replace Conflicts with Breaks.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Fabrizio Regalli ]
  * Imported Upstream version 0.9.9
  * d/copyright:
    + Updated using copyright-format 1.0
    + Changed Upstream-Maintainer in Upstream-Contact
    + Changed Upstream-Source in Source
    + Updated both licenses text
    + Added Martín Ferrari to debian/* copyright files
    + Changed "|" with "or" in license type
    + Added myself to debian/* copyright files
    + Updated year
  * Updated d/compat to 8
  * Updated debhelper to (>= 8)
  * Bump Standards-Version to 3.9.3
  * Added myself to Uploaders
  * Changed Homepage in d/control using default search.cpan.org
  * Added libnetaddr-ip-perl in B-D-I and Depends
  * Removed {Breaks,Replaces} psad (<= 2.1.2-1) in d/control

  [ gregor herrmann ]
  * debian/watch: fix CPAN URL, and drop the other location.
  * Drop (build) dependency on libnetwork-ipv4addr-perl.

 -- Fabrizio Regalli <fabreg@fabreg.it>  Mon, 27 Feb 2012 16:54:38 +0100

libiptables-chainmgr-perl (0.9-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Martín Ferrari ]
  * New upstream release
  * debian/control: Added myself to uploaders.
  * debian/watch: Added CPAN URL.

 -- Martín Ferrari <tincho@debian.org>  Sat, 14 Feb 2009 16:12:38 -0200

libiptables-chainmgr-perl (0.8-1) unstable; urgency=low

  * New upstream release
  * Bumped up Standards-Version to 3.8.0 (no changes).
  * Updated build-dependencies for libiptables-parse-perl (>= 0.7).
  * Updated dependencies for libiptables-parse-perl (>=0.7).
  * Updated debian/copyright according to:
      http://wiki.debian.org/Proposals/CopyrightFormat

 -- Franck Joncourt <franck.mail@dthconnex.com>  Sat, 18 Oct 2008 11:24:44 +0200

libiptables-chainmgr-perl (0.7-1) unstable; urgency=low

  * Initial release (Closes: #481544)

 -- Franck Joncourt <franck.mail@dthconnex.com>  Sun, 18 May 2008 17:23:32 +0200
